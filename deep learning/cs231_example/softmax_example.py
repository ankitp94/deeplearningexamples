__author__ = 'ankit'

# initialize parameters randomly
import numpy as np
import data_util
import  softmax_trainer
import neural_network_trainer
N = 100 # number of points per class
D = 2 # dimensionality
K = 3 # number of classes

X,y = data_util.generate_spiral_data(N,D,K)

W = 0.01 * np.random.randn(D,K)
b = np.zeros((1,K))

# some hyperparameters
step_size = 1e-0
reg = 1e-3 # regularization strength

W,b = softmax_trainer.train_softmax_classifier(X_data=X,y_label=y,reg_param=reg,step_size=step_size,init_bias=b,init_weights=W)

scores = np.dot(X, W) + b
predicted_class = np.argmax(scores, axis=1)
print 'training accuracy: %.2f' % (np.mean(predicted_class == y))

# data_util.plot_boundary(X,y,W,b)

print "Training Neural network"
h=100

W,b,W2,b2= neural_network_trainer.train_neural_network(X_data=X,h=h,y_label=y,step_size=step_size,reg_param=reg,num_class=K)
hidden_layer = np.maximum(0, np.dot(X, W) + b)
scores = np.dot(hidden_layer, W2) + b2
predicted_class = np.argmax(scores, axis=1)

print 'training accuracy: %.2f' % (np.mean(predicted_class == y))

data_util.plot2Layer_NN(X,y,W,b,W2,b2)
__author__ = 'ankit'

# Train a Linear Classifier
import numpy as np


# gradient descent loop
def train_softmax_classifier(X_data, y_label, init_weights, init_bias, step_size, reg_param):
    W = init_weights
    b = init_bias
    reg = reg_param
    X = X_data
    y = y_label
    num_examples = X.shape[0]

    for i in xrange(200):

        # evaluate class scores, [N x K]
        scores = np.dot(X, W) + b

        # compute the class probabilities
        exp_scores = np.exp(scores)
        probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)  # [N x K]

        # compute the loss: average cross-entropy loss and regularization
        corect_logprobs = -np.log(probs[range(num_examples), y])
        data_loss = np.sum(corect_logprobs) / num_examples
        reg_loss = 0.5 * reg * np.sum(W * W)
        loss = data_loss + reg_loss
        if i % 10 == 0:
            print "iteration %d: loss %f" % (i, loss)

        # compute the gradient on scores
        dscores = probs
        dscores[range(num_examples), y] -= 1
        dscores /= num_examples

        # backpropate the gradient to the parameters (W,b)
        dW = np.dot(X.T, dscores)
        db = np.sum(dscores, axis=0, keepdims=True)

        dW += reg * W  # regularization gradient

        # perform a parameter update
        W += -step_size * dW
        b += -step_size * db
    return W, b

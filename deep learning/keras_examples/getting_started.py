from __future__ import absolute_import
from __future__ import print_function

__author__ = 'ankit'

import cPickle
import numpy as np

from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.optimizers import SGD, Adadelta, Adagrad
from keras.utils import np_utils, generic_utils
# from six.moves import range
from keras.datasets import cifar10


def loadData():
    def unpickle(file):
        fo = open(file, 'rb')
        dict = cPickle.load(fo)
        fo.close()
        return dict
    import os
    from os.path import dirname
    dir = os.getcwd()
    cf10_data_dir  =os.path.join(dirname(dirname(dirname(dir))),"dataset","cifar-10-batches-py","*_*")
    # print cf10_data_dir
    import glob
    files = glob.glob(cf10_data_dir)
    # print files
    datasets= []
    testset = []
    for file in files:
        data = unpickle(file)
        if not "test" in file:
            datasets.append(data)
        else:
            testset = data
    return (datasets,testset)


# (d,testData) = loadData()
# X = testData['data']
# Y = np.array(testData['labels'])
# Y.shape= (10000,1)

(X_train, y_train), (X_test, y_test) = cifar10.load_data()
batch_size = 32
nb_classes = 10
nb_epoch = 3
data_augmentation = True


print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

X_train.shape = (50000,3072)
from keras.utils import np_utils, generic_utils

Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

from keras.models import Sequential
model = Sequential()

from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD



layer1 = Dense(3072,100,init='uniform')
model.add(layer1)
model.add(Activation('tanh'))
model.add(Dropout(0.5))

layer2 = Dense(100,50)
model.add(layer2)
model.add(Activation('tanh'))
model.add(Dropout(0.5))

layer3 = Dense(50,10)
model.add(layer3)
model.add(Activation('tanh'))
model.add(Dropout(0.5))


sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='mean_squared_error', optimizer=sgd)


model.fit(X_train, Y_train, nb_epoch=nb_epoch, batch_size=16)
score = model.evaluate(X_train, Y_train, batch_size=16)


